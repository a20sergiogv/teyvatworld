<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PersonajeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

// Perfil de usuario
Route::get('/perfil/{usuario}', [UserController::class, 'show'])->name('user.profile');
Route::get('/perfil/{usuario}/edit', [UserController::class, 'edit'])->name('user.edit');
Route::put('/perfil/{usuario}/edit', [UserController::class, 'update'])->name('user.update');


// Ruta para los personajes
Route::resource('personajes', PersonajeController::class);

// Rutas para los materiales
Route::get('/materiales', [PersonajeController::class, 'listadoMateriales'])->name('personajes.listadomateriales');
Route::get('/materiales/create', [PersonajeController::class, 'createMaterial'])->name('personajes.creatematerial')->middleware('auth.admin');
Route::post('/materiales/create', [PersonajeController::class, 'storeMaterial'])->name('personajes.storematerial')->middleware('auth.admin');
Route::get('/listarmateriales/{material}', [PersonajeController::class, 'showMaterial'])->name('personajes.showmaterial');
Route::get('/listarmateriales/{material}/edit', [PersonajeController::class, 'editMaterial'])->name('personajes.editmaterial')->middleware('auth.admin');
Route::put('/listarmateriales/{material}/edit', [PersonajeController::class, 'updateMaterial'])->name('personajes.updatematerial')->middleware('auth.admin');
Route::delete('/listarmateriales/{material}/edit', [PersonajeController::class, 'destroyMaterial'])->name('personajes.destroymaterial')->middleware('auth.admin');

// Rutas para las ascensiones
Route::get('/ascensiones', [PersonajeController::class, 'listadoAscensiones'])->name('personajes.listadoascensiones');
Route::get('/ascensiones/create', [PersonajeController::class, 'createAscension'])->name('personajes.createascension')->middleware('auth.admin');
Route::post('/ascensiones/create', [PersonajeController::class, 'storeAscension'])->name('personajes.storeascension')->middleware('auth.admin');
Route::get('/listarascensiones/{ascension}/edit', [PersonajeController::class, 'editAscension'])->name('personajes.editascension')->middleware('auth.admin');
Route::put('/listarascensiones/{ascension}/edit', [PersonajeController::class, 'updateAscension'])->name('personajes.updateascension')->middleware('auth.admin');
Route::delete('/listarascensiones/{ascension}/edit', [PersonajeController::class, 'destroyAscension'])->name('personajes.destroyascension')->middleware('auth.admin');

// Ruta para las noticias y sus comentarios
Route::resource('noticias', PostController::class);
Route::post('/comentario/store', [CommentController::class, 'store'])->name('comentario.store');
Route::post('/respuesta/store', [CommentController::class, 'replyStore'])->name('respuesta.store');
Route::delete('/comentario/destroy/{comentario}', [CommentController::class, 'destroy'])->name('comentario.destroy');

