@csrf
<table class="table">
<tr>
        <td><label for="imagen" class="form-label">Imagen de perfil:</label> </td>
        <td>
           <i>Nueva imagen:</i><br/> <input type="file" class="form-control mb-3" id="profile_photo_path" name="profile_photo_path" value="{{ old('profile_photo_path',$usuario->profile_photo_path) }}" maxlength="150">
 
            @if (old('profile_photo_path',$usuario->profile_photo_path)) <!-- Si existe imagen anterior, se muestra el nombre -->
                <i >Imagen anterior:</i><br/>  <img style="margin-bottom:10px;" src='{{ URL::asset("storage/$usuario->profile_photo_path") }}' HEIGHT="100px" /> 
            @endif
            @if ($errors->has('profile_photo_path'))
            <small class='alert alert-danger'>{{ $errors->first('profile_photo_path') }}</small>
            @endif
        </td>
    </tr>
    <tr>
        <td><label for="nombre" class="form-label">Nombre real:</label></td>
        <td>
            <input type="text" class="form-control" id="real_name" name="real_name" value="{{ old('real_name',$usuario->real_name) }}" maxlength="100">
            @error('real_name')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
   
    <tr>
        <td><label for="resumen" class="form-label">Ubicación:</label><br></td>
        <td>
            <input type="text" class="form-control" id="location" name="location" value="{{ old('location',$usuario->location) }}" maxlength="100">
            @error('location')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="texto" class="form-label">ID en el juego:</label><br></td>
        <td>
        <input type="text" class="form-control" id="hoyo_id" name="hoyo_id" value="{{ old('hoyo_id',$usuario->hoyo_id) }}" maxlength="100">
            @error('hoyo_id')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="texto" class="form-label">Fecha de nacimiento:</label><br></td>
        <td>
            <input type="date" class="form-control" id="birthday" name="birthday" value="{{ old('birthday',$usuario->birthday) }}">
            @error('birthday')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
</table>


<script>
    CKEDITOR.replace('texto', {
   removeButtons: 'Source',
    // The rest of options...
} );
</script>
