@extends('plantillas.master')

@section('title')
Olvidaste tu contraseña
@stop

@section('central')
<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <h2>Restablecer contraseña</h2>
        </x-slot>

        <div class="card-body">

            <div class="mb-3">
                {{ __('Escribe aquí tu correo electrónico y te mandaremos un email con el que podrás restablecer tu contraseña.') }}
            </div>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <x-jet-validation-errors class="mb-3" />

            <form method="POST" action="/forgot-password">
                @csrf

                <div class="mb-3">
                    <x-jet-label value="Email" />
                    <x-jet-input type="email" name="email" :value="old('email')" required autofocus />
                </div>

                <div class="d-flex justify-content-end mt-4">
                    <x-jet-button>
                        {{ __('Enviar email') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
@stop