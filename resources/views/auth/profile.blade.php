@extends('plantillas.master')

@section('title')
Usuario: {{$usuario->name}}
@stop

@section('central')
<?php 
        // Obtenemos el id del usuario de la página en la que estamos
        $url = $_SERVER['REQUEST_URI'];
        $separador = "/";

        $arrayURL = explode($separador, $url);

        $idUsuario = $arrayURL[2];
    ?>

<div class="container">
    <!-- Comprobamos que se haya iniciado sesión, y que el id del usuario coincida con el id de la página -->
    @auth 
        @if (auth()->user()->id == $idUsuario)
            <a href="{{ route('user.edit',$usuario) }}" class="btn btn-warning" style="float:right;" title="Editar perfil."><i class="fas fa-cog"></i></a> 
        @endif
    @endauth

    <div class="row mt-5">
        <div class="text-center col" style="margin-top:2px;"><center><div style="width:180px; height:180px;border-radius:50%;overflow:hidden;"><img src='{{ URL::asset("storage/$usuario->profile_photo_path") }}' width="180" /></div></center></div>
        <div class="col-12 col-xl-9">
            <div class="row">
                <div class="row d-flex flex-row bd-highlight">
                   <h1> {{ $usuario->name }}</h1>
                   <hr/>
                </div>
                <div class="row d-flex flex-row bd-highlight">
                   <div style="font-weight:bold;">Nombre real:</div> 
                   <div style="margin-left: 20px;margin-bottom:20px;">
                   @if ( $usuario->location != "") 
                   {{ $usuario->real_name }}
                    @else
                    <i>No especificado.</i>
                   @endif
                </div>
                </div>
                <div class="row d-flex flex-row bd-highlight">
                   <div style="font-weight:bold;">Ubicación:</div> 
                   <div style="margin-left: 20px; margin-bottom:20px;"> 
                   @if ( $usuario->location != "") 
                        {{ $usuario->location }}
                   @else
                        <i>No especificado.</i>
                   @endif
                
                    </div>
                </div>
                <div class="row d-flex flex-row bd-highlight">
                    <div style="font-weight:bold;">UID en el juego:</div> 
                    <div style="margin-left: 20px;margin-bottom:20px;">
                        @if ( $usuario->hoyo_id != "")
                        {{ $usuario->hoyo_id }}
                        @else
                        <i>No especificado.</i>
                        @endif

                    </div>
                </div>
                <div class="row d-flex flex-row bd-highlight">
                    <div style="font-weight:bold;">Fecha de nacimiento:</div> 
                    <div style="margin-left: 20px;margin-bottom:20px;">
                    @if ( $usuario->birthday != "") 
                    <?php echo date("d/m/Y", strtotime($usuario->birthday))  ?>
                   @else
                        <i>No especificado.</i>
                   @endif
                   
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop