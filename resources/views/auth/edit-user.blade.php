@extends('plantillas.master')

@section('title')
Edición de usuario: {{$usuario->name}}
@stop

@section('central')

<?php 
        // Obtenemos el id del usuario de la página en la que estamos
        $url = $_SERVER['REQUEST_URI'];
        $separador = "/";

        $arrayURL = explode($separador, $url);

        $idUsuario = $arrayURL[2];

        // Si el id del usuario logueado no es el mismo que el del usuario que estamos editando, redireccionamos
        if (auth()->user()->id != $idUsuario) {
            header("Location: https://teyvatworld.casacam.net/");
            die();
        }
    ?>

<div class="container pt-3">
<h2>Mi perfil</h2>
<hr/>
<form action="{{ route('user.update', $usuario->id) }}" method="post" enctype="multipart/form-data">
    @METHOD('PUT')
    @include('auth._form')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('user.profile', $usuario) }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Guardar cambios" />
    </div>
</form>
@stop