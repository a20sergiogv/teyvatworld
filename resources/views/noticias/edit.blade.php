@extends('plantillas.master')

@section('title')
Edición de noticia
@stop

@section('central')
<h2>EDICIÓN DE NOTICIA</h2>

<form action="{{ route('noticias.update', $noticia->id) }}" method="post" enctype="multipart/form-data">
    @METHOD('PUT')
    @include('noticias._form')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('noticias.show', $noticia) }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Guardar cambios" />
    </div>
</form>
@stop