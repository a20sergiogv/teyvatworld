@csrf
<table class="table">
    <tr>
        <td><label for="nombre" class="form-label">Nombre de la noticia:</label></td>
        <td>
            <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$noticia->nombre) }}" maxlength="100">
            @error('nombre')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr class="mb-3">
        <td><label for="imagen" class="form-label">Imagen:</label> </td>
        <td>
            <i>Nueva imagen:</i><br/> <input type="file" class="form-control mb-3" id="imagen" name="imagen" value="{{ old('imagen',$noticia->imagen) }}" maxlength="150">
            @if (old('imagen',$noticia->imagen)) <!-- Si existe imagen anterior, se muestra  -->
                <i >Imagen anterior:</i><br/> <img style="margin-bottom:10px;" src='{{ URL::asset("storage/$noticia->imagen") }}' HEIGHT="100px" /> 
            @endif
            @if ($errors->has('imagen'))
            <small class='alert alert-danger'>{{ $errors->first('imagen') }}</small>
            @endif
        </td>
    </tr>
    <tr class="mt-3">
        <td><label for="resumen" class="form-label">Resumen:</label><br></td>
        <td>
            <textarea class="form-control" name="resumen" id="resumen" rows="5" cols="50">{{ old('resumen',$noticia->resumen) }}</textarea>
            @error('resumen')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="texto" class="form-label">Texto:</label><br></td>
        <td>
            <textarea class="form-control" name="texto" id="texto" rows="10" cols="50">{{ old('texto',$noticia->texto) }}</textarea>
            @error('texto')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
</table>


<script>
    CKEDITOR.replace('texto', {
   removeButtons: 'Source',
    // The rest of options...
} );
</script>
