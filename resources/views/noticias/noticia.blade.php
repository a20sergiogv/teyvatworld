@extends('plantillas.master')

@section('title')
{{ $noticia->nombre }}
@stop

@section('central')

<!-- Se o usuario está rexistrado e é admin móstranse opcións -->
@auth
  @if (auth()->user()->role == "admin")
    <div class="btnDerecha">
    <a href="{{ route('noticias.edit',$noticia) }}" class="btn btn-warning" title="Editar esta noticia."><i class="fas fa-edit"></i></a> 
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#ModalBorrar" title="Borrar esta noticia."><i class="fas fa-trash"></i></button>
    </div>
  @endif
@endauth
<style>
  .volver{
    background-color: #fba4a4;
    display: flex;
  align-items: center;
    text-align: center;
    border-radius: 10px;
    color:white;
    margin-top: 50px;
  }

  .volver:hover{
    cursor:pointer;
    background-color: #f96767;
  }

  .volver a{
    text-decoration: none;
    color: white;
    display:block;
    padding: 20px;
  }

  .comentario{
    background-color: #dddddd;
    border-radius: 10px;
    padding: 10px;
    margin-top: 10px;
  }

  .cabeceraComentario{
    font-size: 20px;
    display: flex;
    align-items: center;
  }

  .cuerpoComentario{
    margin-top: 10px;
  }

  .formComentario{
    margin-bottom: 20px;

  }

  .btnResponder {
    background-color: #00c900;
    border-radius: 5px;
    color: white;
    border: none;
  }

  .btnResponder:hover {
    background-color: #039903;
  }
</style>
<!-- Cuerpo de la noticia -->
<div class="container mb-5">
    <h1 style="margin-top:50px;">{{ $noticia->nombre }}</h1>
    <div style="margin-top: 10px; font-size: 18px; font-style: italic;">{{ $noticia->resumen }}</div>
    <p style="font-size: 12px; text-align: right;">Publicado el {{$noticia->updated_at->format('d/m/y')}}

    <center><img src='{{ URL::asset("storage/$noticia->imagen") }}' width="100%" /></center>
    <div class="pt-5">{!! $noticia->texto !!}</div>
</div>

<!-- <div class="d-flex justify-content-center">
  <div class="volver"><a href='{{  route('noticias.index')  }}'>Volver a la lista de noticias</a></div>
</div> -->

<hr>

<!-- Región de comentarios -->
@include('noticias._comentarios')

<!-- Modal para borrar una noticia -->
<div class="modal fade" id="ModalBorrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Borrar</h5>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea realmente borrar esta noticia?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <form action="{{ route('noticias.destroy',$noticia) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-danger" value="Borrar" /> </form>      </div>
    </div>
  </div>
</div>


@stop