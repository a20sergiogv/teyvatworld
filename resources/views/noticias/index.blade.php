@extends('plantillas.master')

@section('title')
Noticias
@stop

@section('central')
<div class="info">
    <h2>Noticias</h2>
    <div>Todas las noticias más recientes.</div>
</div>

<!-- Si el usuario está registrado y es admin... -->
@auth
    @if (auth()->user()->role == "admin")
        <div class="btnDerecha">
            <a class="btn btn-secondary " href="{{ route('noticias.create') }}" title="Crear nueva noticia."><i class="fas fa-plus-circle"></i></a>
        </div>
    @endif
@endauth
<style>
    .divNoticia{
        padding: 10px;
        margin-bottom: 5px;
        background-color: #e9cbcb;
        border-radius: 10px;
    }

    .divNoticia:hover{
        background-color: #e9aeae;
        cursor:pointer;
    }

</style>

<!-- Lista de noticias -->
@foreach($noticias as $noticia)
    <div class="container">
        <div class="row divNoticia" onclick="window.location.href='{{ route('noticias.show',$noticia) }}'">
            <div class="text-center col" style="margin-top:2px;"><center><img style="max-height: 100px;" src='{{ URL::asset("storage/$noticia->imagen") }}'  /></center></div>
            <div class="col-12 col-md-8">
                <div class="row">
                    <div class="row d-flex flex-row bd-highlight ">
                        <div class="col-12" style="font-size: 20px; padding:10px;">{{ $noticia->nombre }}</div>
                    </div>
                    <div class="row d-flex flex-row bd-highlight">
                        <div class="col-12" style="font-size: 12px; padding:10px;">{{ $noticia->resumen }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
    

{!! $noticias->links() !!}





@stop