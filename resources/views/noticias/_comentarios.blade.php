

<div class="container">
   <!-- Formulario para comentar, los usuarios anónimos no podrán comentar -->
 <div class="formComentario">
    @guest
        <center><p style="font-style:italic"><a href="{{ route('login') }}">Inicia sesión</a> para poder comentar.</p></center>
    @endguest
    
    @auth
      <center><h3 style="font-size: 20px; margin: 20px;">¡Déjanos tu comentario!</h3></center>
      
          <form action="{{ route('comentario.store') }}" method="post">
            <input type="hidden" name="noticia_id" value="{{ $noticia->id }}">
          <textarea class="form-control" name="texto" id="texto" rows="5" cols="50"  placeholder="Comparte lo que piensas"></textarea>
          @csrf
            <div style="text-align: right; margin-top: 10px;">
                <input type="submit" class="btnResponder" value="Publicar comentario" />
            </div>
        </form>
    </li>
    @endauth
  </div>

  <h2 style="font-size:22px; border: 1px solid black; border-radius: 10px; padding: 10px; margin-bottom: 20px;">Todos los comentarios</h2>

  <!-- Lista de comentarios -->
  @foreach($noticia->comments as $comentario)
    <div class="comentario">
      <div class="cabeceraComentario">
      <div style="width:32px; height:32px;border-radius:50%;overflow:hidden; display:inline"><img src='/storage/{{$comentario->user->profile_photo_path}}' style="width:32px" /></div> <a style="margin-left:10px; margin-right:10px; text-decoration:none;" href="{{ route('user.profile', $comentario->user->id ) }}" role="button" aria-haspopup="true" aria-expanded="false"> {{$comentario->user->name}}  </a><span style="font-size: 14px; font-style:italic;">el <?php echo date("d/m/Y", strtotime($comentario->created_at))  ?></span> @auth @if (auth()->user()->role == "admin")<button type="button" class="btn" data-toggle="modal" data-target="#ModalBorrarComentario" title="Eliminar este comentario."  style="margin-left: 10px;"><i class="fas fa-trash"></i></button>@endif @endauth
        
            <!-- Modal para borrar un comentario -->
            <div class="modal fade" id="ModalBorrarComentario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Borrar</h5>
                    </button>
                  </div>
                  <div class="modal-body">
                    ¿Desea realmente borrar este comentario? 
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <form action="{{ route('comentario.destroy',$comentario) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-danger" value="Borrar" /> </form>      </div>
                </div>
              </div>
            </div>
      </div>
      <div class="cuerpoComentario">
        {{$comentario->texto}}
       
      </div>
        @foreach($comentario->answers as $respuesta)
        <hr/>
        <div class="comentario" style="margin-left: 20px;">
          <div class="cabeceraComentario">
          <div style="width:32px; height:32px;border-radius:50%;overflow:hidden; display:inline"><img src='/storage/{{$respuesta->user->profile_photo_path}}' style="width:32px" /></div> <a style="margin-left:10px; margin-right:10px; text-decoration:none;" href="{{ route('user.profile', $respuesta->user->id ) }}" role="button" aria-haspopup="true" aria-expanded="false"> {{$respuesta->user->name}}  </a><span style="font-size: 14px; font-style:italic;">el <?php echo date("d/m/Y", strtotime($respuesta->created_at))  ?></span> @auth @if (auth()->user()->role == "admin")<button type="button" class="btn" data-toggle="modal" data-target="#ModalBorrarRespuesta" title="Eliminar este comentario."  style="margin-left: 10px;"><i class="fas fa-trash"></i></button>@endif @endauth
        

            <div class="modal fade" id="ModalBorrarRespuesta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Borrar</h5>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea realmente borrar esta respuesta?  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <form action="{{ route('comentario.destroy',$respuesta) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-danger" value="Borrar" /> </form>      </div>
    </div>
  </div>
</div>
          </div>
          <div class="cuerpoComentario">
            <div>{{ $respuesta->texto }}</div>
            
          </div>
          
    </div>

    <!-- Modal para borrar la respuesta de un comentario -->


        
        @endforeach
        
        <!-- Responder un comentario -->
        @guest
    @endguest
    @auth
         <form action="{{ route('respuesta.store') }}" method="post" style="margin-top: 10px;">
            <input type="hidden" name="noticia_id" value="{{ $noticia->id }}">
            <input type="hidden" name="comentario_id" value="{{ $comentario->id }}">
          <textarea class="form-control" name="texto" id="texto" rows="3" cols="20" placeholder="Continuar esta conversación"></textarea>
          @csrf
            <div style="text-align: right; margin-top: 10px;">
                <input type="submit" class="btnResponder" value="Responder" />
            </div>
        </form>
      @endauth
    </div>



  @endforeach

  

