@extends('plantillas.master')

@section('title')
Creación de noticia
@stop

@section('central')
<h2>CREACIÓN DE NOTICIA</h2>
<form action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
    @include('noticias._form')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('noticias.index') }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Crear noticia" />
    </div>
</form>



@stop