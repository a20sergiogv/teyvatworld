<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Clase DWCS - 2021-2022">
    <meta name="generator" content="">
    <title>@yield('title') | TeyvatWorld</title>
 
    <link href="https://bootswatch.com/5/slate/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="//cdn.ckeditor.com/4.19.0/full/ckeditor.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
    <!-- Favicons -->
    <!-- <link rel="apple-touch-icon" href="/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon.ico"> -->

    <meta name="theme-color" content="#7952b3">

    <!-- Estilos CSS para esta página -->
    <style>
    
        body {
          background: url(https://www.nawpic.com/media/2020/genshin-impact-nawpic-11.jpg);
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-size: cover;
         
    
    /* background-color: lightgrey; */
            margin: 0;
            padding-top: 4.5rem;
        }
        
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .footer{
          font-size: 12px;
          margin-top: 50px;
          box-shadow: 0 0 3px 3px white;
        }

        div.btnDesplegable {
          text-decoration:none;
          width: 100%;
          cursor: pointer;
          padding: 10px;
        }

        div.btnDesplegable:hover{
          background-color: grey;
        }

        .navLink{
          text-decoration: none;
          display: flex;
          align-items: center;
          padding: 0 10px;
          color: white;
          height: 50px;
          
        }

        .navLink:hover{
          background-color: grey;
          color: white;
        }

        .navUser{
          border: 1px solid white;
          margin-left: 5px;
          border-radius: 10px;
        }

        .contenedor{
          width: 80%;
          margin-left: auto;
          margin-right: auto;
          margin-bottom: auto;
          
        }

        /* .modal-content {
          opacity: 1 !important;
    background-clip: padding-box;
    border: none;
}

.modal-backdrop {
 
    display: none;    
} */

        .filaInfo{
          margin-top:1px;
          font-weight: bold;
          padding-right: 0px;
          padding-left: 0px;
          margin-right: 0px;
          margin-left: 0px;
        }

        .filaInfo:hover{
          background-color: lightgrey;
        }

        .zonaCentral{
          background-color: rgba(255, 255, 255, 0.9);
          width: 100%;
          padding: 10px;
          padding-bottom: 50px;
         
        }

      
        @media (max-width: 450px) {
          .contenedor{
            width: 100%;
            
          }
          .zonaCentral{
           
            padding-left: 0px;
            padding-right: 0px;
          }
        }

        .tablaAscensiones {
          min-width: 1000px;
        }

        .info{
          background-color: #d1cccc;
          padding: 20px;
          margin-left: -12px;
          margin-right: -12px;
          margin-bottom: 10px;        }

          div.divPJ{
        display: flex;
        align-items: center;
        background-color: grey;
        border:2px solid white;
        color:white;
        height: 60px;
        cursor: pointer;
        font-weight: bold;
    }

    div.divPJ:hover{
        background-color: darkgrey;
        transform: scale(1.1);
    }

    .btnDerecha{
        text-align: right;
        margin-bottom: 20px;
    }

    table.creacion{
        width: 100%;
    }

        
    </style>
</head>

<body class="d-flex flex-column min-vh-100">
  <!-- Cabecera -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-dark" style="background:  rgba(0, 0, 0, .7) ">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('inicio') }}">Teyvat World</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav me-auto">
        <li class="navLi">
            <a class="navLink" href="{{ route('noticias.index') }}">Noticias</a>
          </li>
          <li class="navLi">
            <a class="navLink" href="{{ route('personajes.index') }}">Personajes</a>
          </li>
          <li class="navLi">
            <a class="navLink" href="{{ route('personajes.listadomateriales') }}">Materiales</a>
          </li>
          <li class="navLi">
            <a class="navLink" href="{{ route('personajes.listadoascensiones') }}">Ascensiones</a>
          </li>
          
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Separated link</a>
            </div>
          </li> -->
        </ul>

        <!-- En función de si hay o no sesión iniciada, se mostrarán diferentes opciones -->
        <ul class="nav navbar-nav ms-auto">
          @guest
          <li class="d-flex nav-item">
            <a class="navLink navUser" href="{{ route('login') }}" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i>&nbsp Iniciar sesión </a>
          </li>
          @endguest
          @auth
          <li class="nav-item dropdown">
            
              <a href="#" class="navLink navUser dropdown-toggle" data-bs-toggle="dropdown"> <div style="width:32px; height:32px;border-radius:50%;overflow:hidden;"><img src='/storage/{{auth()->user()->profile_photo_path}}' style="width:32px" /></div>&nbsp {{ auth()->user()->name }}</a>
              <div class="dropdown-menu dropdown-menu-end">
              <div class="btnDesplegable"><a style="color: black; text-decoration:none;" href="{{ route('user.profile', auth()->user()->id ) }}" role="button" aria-haspopup="true" aria-expanded="false"> Perfil </a></div>
                <div class="btnDesplegable" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar sesión</div>
              </div>
          </li>
          @endauth
        </ul>
        <!-- <form class="d-flex">
          <input class="form-control me-sm-2" type="text" placeholder="Search">
          <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
        </form> -->
      </div>
    </div>
  </nav>

  <!-- Cuerpo de la página -->
  <main class="contenedor" >
    <div class="rounded zonaCentral" style="margin-top: 20px;">
      @yield('central')
    </div>
  </main>

  <!-- Footer -->
  <div class="footer container-fluid bg-light" >
    <div class="row pb-3">
    <div class="pt-2 col-12 col-md-9" >
        <div style="font-size: 20px;">Sobre nosotros</div>
          <div style="line-height: 10px;">
          Copyright 2022 TeyvatWorld.Casacam.Net
            <br><br>
            TeyvatWorld.Casacam.Net no está afiliado ni relacionado con miHoYo.
            <br><br>
            TeyvatWorld.Casacam.Net proporciona información y herramientas útiles para el juego Genshin Impact, disponible en PC, Playstation 4 y móviles con sistema operativo iOS y Android.  
          </div>
        </div>
    
      <div class="pt-2 col-12 col-md-3">
        <div style="font-size: 20px;">Otros enlaces</div>
          <div >
            <a href="#">Contacto</a>
            <br>
            <a href="#">Política de privacidad</a>
            <br>
            <a href="#">Términos y condiciones</a>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  <!-- Esto es necesario para el logout -->
  <form method="POST" id="logout-form" action="{{ route('logout') }}">
      @csrf
  </form>

  <!-- JavaScript -->
  <script src="{{ asset('js/app.js') }}"></script>

</body>

</html>