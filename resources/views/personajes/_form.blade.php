@csrf
<table class="table">
    <tr>
        <td><label for="nombre" class="form-label">Nombre del personaje:</label></td>
        <td>
            <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$personaje->nombre) }}" maxlength="30">
            @error('nombre')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>

    <tr>
        <td><label for="imagen" class="form-label">Imagen:</label> </td>
        <td>
            <i>Nueva imagen:</i><br/> <input type="file" class="form-control mb-3" id="imagen" name="imagen" value="{{ old('imagen',$personaje->imagen) }}" >
            @if (old('imagen',$personaje->imagen)) <!-- Si existe imagen anterior, se muestra  -->
                <i >Imagen anterior:</i><br/> <img style="margin-bottom:10px;" src='{{ URL::asset("storage/$personaje->imagen") }}' HEIGHT="100px" /> 
            @endif
            @if ($errors->has('imagen'))
            <small class='alert alert-danger'>{{ $errors->first('imagen') }}</small>
            @endif
        </td>
    </tr>

    <tr>
        <td><label for="titulo" class="form-label">Título:</label></td>
        <td>
            <input type="text" class="form-control" id="titulo" name="titulo" value="{{ old('titulo',$personaje->titulo) }}" maxlength="50">
            @error('titulo')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>

    <tr>
        <td><label for="alianza" class="form-label">Alianza:</label></td>
        <td>
            <input type="text" class="form-control" id="alianza" name="alianza" value="{{ old('alianza',$personaje->alianza) }}" maxlength="50">
            @error('alianza')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="rareza" class="form-label">Rareza:</label></td>
        <td>
            <input type="text" class="form-control" id="rareza" name="rareza" value="{{ old('rareza',$personaje->rareza) }}" maxlength="30">
            @error('rareza')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="arma" class="form-label">Arma:</label></td>
        <td>
            <input type="text" class="form-control" id="arma" name="arma" value="{{ old('arma',$personaje->arma) }}" maxlength="10">
            @error('arma')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="elemento" class="form-label">Elemento:</label></td>
        <td>
            <input type="text" class="form-control" id="elemento" name="elemento" value="{{ old('elemento',$personaje->elemento) }}" maxlength="10">
            @error('elemento')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="cumple" class="form-label">Día de nacimiento:</label></td>
        <td>
            <input type="text" class="form-control" id="cumple" name="cumple" value="{{ old('cumple',$personaje->cumple) }}" maxlength="30">
            @error('cumple')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="constelacion" class="form-label">Constelación:</label></td>
        <td>
            <input type="text" class="form-control" id="constelacion" name="constelacion" value="{{ old('constelacion',$personaje->constelacion) }}" maxlength="30">
            @error('constelacion')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="S_J" class="form-label">Seiyuu en japonés:</label></td>
        <td>
            <input type="text" class="form-control" id="S_J" name="S_J" value="{{ old('S_J',$personaje->S_J) }}" maxlength="30">
            @error('S_J')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="S_C" class="form-label">Seiyuu en chino:</label></td>
        <td>
            <input type="text" class="form-control" id="S_C" name="S_C" value="{{ old('S_C',$personaje->S_C) }}" maxlength="30">
            @error('S_C')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="S_I" class="form-label">Seiyuu en inglés:</label></td>
        <td>
            <input type="text" class="form-control" id="S_I" name="S_I" value="{{ old('S_I',$personaje->S_I) }}" maxlength="30">
            @error('S_I')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
    <tr>
        <td><label for="descripcion" class="form-label">Descripción:</label><br></td>
        <td>
            <textarea class="form-control" name="descripcion" id="descripcion" rows="10" cols="50" maxlength="200">{{ old('descripcion',$personaje->descripcion) }}</textarea>
            @error('descripcion')
            <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
        </td>
    </tr>
</table>
