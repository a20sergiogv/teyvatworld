@extends('plantillas.master')

@section('title')
Crear ascensión
@stop

@section('central')
<h2>Crear ascensión</h2>
<form action="{{ route('personajes.storeascension') }}" method="post">
    @include('personajes._formAscension')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('personajes.listadoascensiones') }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Crear ascensión" />
    </div>
</form>

@stop