@csrf
<div class="mb-3">
    <label for="nombre" class="form-label">Nombre del material:</label>
    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$material->nombre) }}" maxlength="30">
    @error('nombre')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="imagen" class="form-label">Imagen:</label> <strong>{{ old('imagen',$material->imagen) }}</strong>
    <input type="file" class="form-control" id="imagen" name="imagen" value="{{ old('imagen',$material->imagen) }}" maxlength="150">
    @if ($errors->has('imagen'))
    <small class='alert alert-danger'>{{ $errors->first('imagen') }}</small>
    @endif
</div>

<div class="mb-3">
    <label for="tipo" class="form-label">Tipo:</label>
    <input type="text" class="form-control" id="tipo" name="tipo" value="{{ old('tipo',$material->tipo) }}" maxlength="40">
    @error('tipo')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="rareza" class="form-label">Rareza:</label>
    <input type="text" class="form-control" id="rareza" name="rareza" value="{{ old('rareza',$material->rareza) }}" maxlength="15">
    @error('rareza')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="ubicacion" class="form-label">Ubicación:</label>
    <input type="text" class="form-control" id="ubicacion" name="ubicacion" value="{{ old('ubicacion',$material->ubicacion) }}" maxlength="50">
    @error('ubicacion')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="descripcion" class="form-label">Descripción:</label><br>
    <textarea class="form-control" name="descripcion" id="descripcion" rows="10" cols="50" maxlength="500">{{ old('descripcion',$material->descripcion) }}</textarea>
    @error('descripcion')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>