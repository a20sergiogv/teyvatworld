@extends('plantillas.master')

@section('title')
Creación de material
@stop

@section('central')
<h2>CREACIÓN DE MATERIAL</h2>
@if(Session::has('mensaje'))
<div class="alert {{ Session::get('alert-class') }}">
    {{ Session::get('mensaje') }}
</div>
@endif
<form action="{{ route('personajes.storematerial') }}" method="post" enctype="multipart/form-data">
    @include('personajes._formMaterial')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('personajes.listadomateriales') }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Crear material" />
    </div>
</form>
@stop