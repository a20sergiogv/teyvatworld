@extends('plantillas.master')

@section('title')
Edición de personaje
@stop

@section('central')
<h2>EDICIÓN DE PERSONAJE</h2>

<form action="{{ route('personajes.update', $personaje->id) }}" method="post" enctype="multipart/form-data">
    @METHOD('PUT')
    @include('personajes._form')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('personajes.show', $personaje) }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Guardar cambios" />
    </div>
</form>
@stop