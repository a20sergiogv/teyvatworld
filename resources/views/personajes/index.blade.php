@extends('plantillas.master')

@section('title')
Listado de personajes
@stop

@section('central')
<div class="info">
    <h2>LISTADO DE PERSONAJES</h2>
    <p>Aquí encontrarás una lista de todos los personajes del juego.</p>
</div>

<!-- Si el usuario está registrado y es admin... -->
@auth
    @if (auth()->user()->role == "admin")
    <div class="btnDerecha">
        <a class="btn btn-secondary " href="{{ route('personajes.create') }}" title="Crear nuevo personaje."><i class="fas fa-plus-circle"></i></a>
    </div>

    @endif
@endauth


<!-- Lista de personajes -->
<div class="container">
    <div class="row">
    @foreach($personajes as $personaje)
        <div class="divPJ col-md-6 col-xl-4" onclick="window.location.href='{{ route('personajes.show',$personaje) }}'">
            <div><img src='{{ URL::asset("storage/$personaje->imagen") }}' width="50" />  {{ $personaje->nombre }}</div>
        </div>
        @endforeach
    </div>
</div>

@stop