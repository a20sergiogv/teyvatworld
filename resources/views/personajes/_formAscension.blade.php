@csrf
<div class="mb-3">
    <label for="material_id" class="form-label">ID del material</label> 
    <select name="material_id" id="material_id" class="form-control">
        <option value="0">ID del material:</option>
        @foreach ($materiales as $material)
            <option value="{{  $material->id }}" @if ($material->id === old('material_id',$ascension->material_id) ) selected @endif>{{ $material->id }} - {{ $material->nombre }}</option>
        @endforeach
    </select>
    @error('material_id')
    <small class="alert alert-danger">{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="personaje_id" class="form-label">ID del personaje</label>
    <select name="personaje_id" id="personaje_id" class="form-control">
        <option value="0">ID del personaje:</option>
        @foreach ($personajes as $personaje)
            <option value="{{  $personaje->id }}"  @if ($personaje->id === old('personaje_id',$ascension->personaje_id) ) selected @endif>{{ $personaje->id }} - {{ $personaje->nombre }}</option>
        @endforeach
    </select>
    @error('personaje_id')
    <small class="alert alert-danger">{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="oro" class="form-label">Oro requerido:</label>
    <input class="form-control" type="number" name="oro" id="oro" max="999999" value="{{ old('oro',$ascension->oro) }}">
    @error('oro')
    <small class="alert alert-danger">{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="cantidad" class="form-label">Cantidad requerida:</label>
    <input class="form-control" type="number" name="cantidad" id="cantidad" max="999999"  value="{{ old('cantidad',$ascension->cantidad) }}">
    @error('cantidad')
    <small class="alert alert-danger">{{ $message }}</small>
    @enderror
</div>