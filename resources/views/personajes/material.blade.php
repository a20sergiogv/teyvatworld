@extends('plantillas.master')

@section('title')
{{ $material->nombre }}
@stop

@section('central')
<!-- Se o usuario está rexistrado e é admin móstranse opcións -->
@auth
@if (auth()->user()->role == "admin")
<div class="btnDerecha">
<a href="{{ route('personajes.editmaterial',$material) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a> 
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#ModalBorrar"><i class="fas fa-trash"></i></button>
</div>
@endif
@endauth


<!-- Tabla con la información -->
<div class="container">
    <div class="row">
        <div class="bg-dark text-white" style="font-size:18px; padding:5px; font-weight: bold;">{{ $material->nombre }}</div>
    </div>

    <div class="row">
        <div class="text-center col" style="margin-top:2px;"><center><img src='{{ URL::asset("storage/$material->imagen") }}' width="180" /></center></div>
        <div class="col-12 col-xl-9">
            <div class="row">
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Tipo</div>
                    <div class="col-12 col-md-9" style="padding:10px;">{{ $material->tipo }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Rareza</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $material->rareza }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Ubicación</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $material->ubicacion }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Descripción</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $material->descripcion }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para borrar -->
<div class="modal fade" id="ModalBorrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Borrar</h5>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea realmente borrar este material?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <form action="{{ route('personajes.destroymaterial',$material) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-danger" value="Borrar" /> </form>      </div>
    </div>
  </div>
</div>

@stop