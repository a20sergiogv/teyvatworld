@extends('plantillas.master')

@section('title')
Creación de personaje
@stop

@section('central')
<h2>CREACIÓN DE PERSONAJE</h2>
<form action="{{ route('personajes.store') }}" method="post" enctype="multipart/form-data">
    @include('personajes._form')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('personajes.index') }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Crear personaje" />
    </div>
</form>
@stop