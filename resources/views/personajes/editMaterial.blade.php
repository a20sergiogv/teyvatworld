@extends('plantillas.master')

@section('title')
Edición de material
@stop

@section('central')
<h2>EDICIÓN DE MATERIAL</h2>

<form action="{{ route('personajes.updatematerial', $material->id) }}" method="post" enctype="multipart/form-data">
    @METHOD('PUT')
    @include('personajes._formMaterial')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('personajes.showmaterial', $material) }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Guardar cambios" />
    </div>
</form>
@stop