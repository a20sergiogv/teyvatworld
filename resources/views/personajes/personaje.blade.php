@extends('plantillas.master')

@section('title')
{{ $personaje->nombre }}
@stop

@section('central')

<!-- Se o usuario está rexistrado e é admin móstranse opcións -->
@auth
@if (auth()->user()->role == "admin")
<div class="btnDerecha">
<a href="{{ route('personajes.edit',$personaje) }}" class="btn btn-warning" title="Editar este personaje."><i class="fas fa-edit"></i></a> 
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#ModalBorrar" title="Borrar este personaje."><i class="fas fa-trash"></i></button>
</div>
@endif
@endauth

<!-- Tabla que mostra os datos -->
<div class="container">
    <div class="row">
        <div class="bg-dark text-white" style="font-size:18px; padding:5px; font-weight: bold;">{{ $personaje->nombre }}</div>
    </div>

    <div class="row">
        <div class="text-center col" style="margin-top:2px;"><center><img src='{{ URL::asset("storage/$personaje->imagen") }}' width="180" /></center></div>
        <div class="col-12 col-xl-9">
            <div class="row">
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-12 col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Título</div>
                    <div class="col-12 col-md-9" style="padding:10px;">{{ $personaje->titulo }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Alianza</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->alianza }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Rareza</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->rareza }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Arma</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->arma }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Elemento</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->elemento }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Día de nacimiento</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->cumple }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Constelación</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->constelacion }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Seiyuu en japonés</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->S_J }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Seiyuu en chino</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->S_C }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Seiyuu en inglés</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->S_I }}</div>
                </div>
                <div class="row d-flex flex-row bd-highlight filaInfo">
                    <div class="col-md-3 bg-secondary text-white fw-bolder" style="padding:10px;">Descripción</div>
                    <div  class="col-12 col-md-9" style="padding:10px;">{{ $personaje->descripcion }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para borrar -->
<div class="modal fade" id="ModalBorrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Borrar</h5>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea realmente borrar este personaje?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <form action="{{ route('personajes.destroy',$personaje) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-danger" value="Borrar" /> </form>      </div>
    </div>
  </div>
</div>

@stop