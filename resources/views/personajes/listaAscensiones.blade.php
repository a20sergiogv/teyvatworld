@extends('plantillas.master')

@section('title')
Listado de ascensiones de personajes
@stop

@section('central')
<div class="info">
    <h2>LISTADO DE ASCENSIONES</h2>
    <div>Aquí encontrarás una lista de todos los materiales requeridos para subir de nivel a tus personajes.</div>
</div>

@auth
    @if (auth()->user()->role == "admin")
    <div class="btnDerecha">
    <a class="btn btn-secondary" href="{{ route('personajes.createascension') }}"><i class="fas fa-plus-circle"></i></a>

    </div>

    @endif
@endauth

<!-- Tabla con las relaciones --> 
<div class="table-responsive">
<table class="table table-hover tablaAscensiones">
    <tbody>
        <tr>
            <th class="bg-dark text-white" scope="row" class="bg-secondary text-white fw-bolder"> Personaje </th>
            <th class="bg-dark text-white" scope="row" class="bg-secondary text-white fw-bolder"> Material </th>
            <th class="bg-dark text-white" scope="row" class="bg-secondary text-white fw-bolder"> Cantidad del material </th>
            <th class="bg-dark text-white" scope="row" class="bg-secondary text-white fw-bolder"> Oro requerido </th>
            @auth @if (auth()->user()->role == "admin") <th class="bg-dark text-white" scope="row" class="bg-secondary text-white fw-bolder"> Acciones </th> @endif @endauth
        </tr>
        @foreach($ascensiones as $ascension)
        <tr class="table-secondary">  
            <tr>
                <td scope="row" class="bg-secondary text-white fw-bolder align-middle"> 
                    @foreach($personajes as $personaje)
                        @if ( $ascension->personaje_id  ===  $personaje->id )
                            <img src='{{ URL::asset("storage/$personaje->imagen") }}' width="50" /> <a href="{{ route('personajes.show',$personaje) }}" style="text-decoration:none; color: white">{{ $personaje->nombre }}</a>
                        @endif
                    @endforeach
                </td>
                <td scope="row" class="bg-secondary text-white fw-bolder align-middle"> 
                    @foreach($materiales as $material)
                        @if ( $ascension->material_id  ===  $material->id )
                            <img src='{{ URL::asset("storage/$material->imagen") }}' width="50" /> <a href="{{ route('personajes.showmaterial',$material) }}" style="text-decoration:none; color: white">{{ $material->nombre }}</a>
                        @endif
                    @endforeach
                </td>
                <td scope="row" class="bg-secondary text-white fw-bolder align-middle"> {{ $ascension->cantidad }}</td>
                <td scope="row" class="bg-secondary text-white fw-bolder align-middle"> {{ $ascension->oro }}</td>
                @auth
                @if (auth()->user()->role == "admin")
                <td scope="row" class="bg-secondary text-white fw-bolder"> 
                    <a href="{{ route('personajes.editascension', $ascension)}}" class="btn btn-warning">Editar</a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Borrar</button>
                </td>
                @endif
                @endauth
            </tr>

        </tr>

        <!-- Modal para borrar -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Borrar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea realmente borrar esta ascensión?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <form action="{{ route('personajes.destroyascension',$ascension) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-danger" value="Borrar" /> </form>      </div>
            </div>
        </div>
        </div>
        <!-- Fin modal -->
        @endforeach
    </tbody>
</table>
</div>




@stop