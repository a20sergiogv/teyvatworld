@extends('plantillas.master')

@section('title')
Editar ascensión
@stop

@section('central')
<h2>EDICIÓN DE ASCENSIÓN</h2>
<form action="{{ route('personajes.updateascension', $ascension->id) }}" method="post">
    @METHOD('PUT')
    @include('personajes._formAscension')
    <div style="text-align: right; margin-top: 10px;">
        <a class="btn btn-secondary" href="{{ route('personajes.listadoascensiones') }}">Cancelar</a>
        <input type="reset" class="btn btn-danger" name="Limpiar" />
        <input type="submit" class="btn btn-success" value="Guardar cambios" />
    </div>
    

</form>

@stop
