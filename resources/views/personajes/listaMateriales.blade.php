@extends('plantillas.master')

@section('title')
Listado de materiales
@stop

@section('central')
<div class="info">
    <h2>LISTADO DE MATERIALES</h2>
    <div>Aquí encontrarás una lista de todos los materiales del juego.</div>
</div>
<style>
    div.divPJ{
        display: flex;
        align-items: center;
        background-color: grey;
        border:2px solid white;
        color:white;
        height: 60px;
        cursor: pointer;
        font-weight: bold;
    }

    div.divPJ:hover{
        background-color: darkgrey;
    }
</style>
@auth

<!-- Si el usuario está registrado y es admin... -->
@auth
    @if (auth()->user()->role == "admin")
    <div class="btnDerecha">
    <a class="btn btn-secondary" href="{{ route('personajes.creatematerial') }}"><i class="fas fa-plus-circle"></i></a>

    </div>

    @endif
@endauth


@endauth
<div class="container">
    <div class="row">
        @foreach($materiales as $material)
        <div class="divPJ col-md-6 col-xl-4" onclick="window.location.href='{{  route('personajes.showmaterial',$material) }}'">
            <div><img src='{{ URL::asset("storage/$material->imagen") }}' width="50" />  {{ $material->nombre }}</div>
        </div>
        @endforeach
    </div>
 
</div>
@stop