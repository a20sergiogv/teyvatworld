<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MaterialPersonaje extends Pivot
{
    use HasFactory;

    protected $fillable = array('material_id', 'personaje_id', 'oro', 'cantidad');
}
