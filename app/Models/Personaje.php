<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personaje extends Model
{
    use HasFactory;
    protected $fillable = array('nombre', 'imagen', 'titulo', 'alianza', 'rareza', 'arma', 'elemento', 'cumple',
    'constelacion', 'S_J', 'S_C', 'S_I', 'descripcion');

    public function materiales()
    {
        return $this->belongsToMany(Material::class);
    }

}

