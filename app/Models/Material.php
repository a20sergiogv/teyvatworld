<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;
    protected $fillable = array('nombre', 'imagen', 'tipo', 'rareza', 'ubicacion', 'descripcion');
    
    public function personajes()
    {
        return $this->belongsToMany(Personaje::class);
    }
}