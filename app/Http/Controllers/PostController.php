<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Support\Facades\Session;
use App\Models\Post;

class PostController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth')->except('create');
        $this->middleware('auth.admin')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Post::orderBy('created_at', 'DESC')->paginate(7);

        return view('noticias.index')->with('noticias', $noticias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('noticias.create')->with('noticia', new Post());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $datosvalidados = $request->validated();

        // Colocamos una imagen por defecto
        $datosvalidados['imagen'] = "imagenes/imagen_por_defecto.jpg";

        // Si recibe una imagen, cambiamos la imagen defecto por la imagen recibida, y la guardamos en storage
        if ($request->file('imagen')){
            $imagen = $request->file('imagen');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['imagen'] = $ruta;
        }

        // Creamos el noticia con todos los datos
        Post::create($datosvalidados);

        // Mostramos mensaje.
        Session::flash('mensaje', 'noticia creado correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('noticias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $noticia)
    {
        // Conseguimos la informacion de la noticia
        return view('noticias.noticia')->with('noticia', $noticia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $noticia)
    {
        return view('noticias.edit')->with('noticia', $noticia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePostRequest $request, Post $noticia)
    {
        $datosvalidados = $request->validated();

        if ($request->file('imagen')){
            // Borramos la imagen anterior, siempre y cuando esta no sea la imagen que configuramos por defecto
            if($noticia->imagen != 'imagenes/imagen_por_defecto.jpg') { unlink(storage_path('app/public/'.$noticia->imagen)); }

            $imagen = $request->file('imagen');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['imagen'] = $ruta;
        }

        // Actualizamos el personaje.
        $noticia->update($datosvalidados);

        // Mostramos un mensaje.
        Session::flash('mensaje', 'Personaje actualizado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('noticias.show',$noticia);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $noticia)
    {
        // Borramos la imagen del servidor, siempre y cuando esta no sea la imagen que configuramos por defecto
        if($noticia->imagen != 'imagenes/imagen_por_defecto.jpg') { unlink(storage_path('app/public/'.$noticia->imagen)); }

        // Borramos el registro
        $noticia->delete();

        // Mostramos mensaje
        Session::flash('mensaje', 'noticia borrado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index
        return redirect()->route('noticias.index');
    }
}
