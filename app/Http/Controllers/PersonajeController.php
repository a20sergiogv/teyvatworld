<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personaje;
use App\Models\Material;
use App\Models\MaterialPersonaje;
use App\Http\Requests\StorePersonajeRequest;
use App\Http\Requests\StoreMaterialRequest;
use App\Http\Requests\StoreAscensionRequest;
use Illuminate\Support\Facades\Session;

class PersonajeController extends Controller
{
    // Agregamos seguridad, para que solo usuarios autenticados puedan acceder a las listas
    public function __construct()
    {
        //$this->middleware('auth')->except('create');
        $this->middleware('auth.admin')->except('index', 'listadoMateriales', 'listadoAscensiones', 'show', 'showMaterial');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personajes = Personaje::All();

        return view('personajes.index')->with('personajes', $personajes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('personajes.create')->with('personaje', new Personaje());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonajeRequest $request)
    {

        $datosvalidados = $request->validated();

        // Colocamos una imagen por defecto
        $datosvalidados['imagen'] = "imagenes/imagen_por_defecto.jpg";

        // Si recibe una imagen, cambiamos la imagen defecto por la imagen recibida, y la guardamos en storage
        if ($request->file('imagen')){
            $imagen = $request->file('imagen');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['imagen'] = $ruta;
        }

        // Creamos el personaje con todos los datos
        Personaje::create($datosvalidados);

        // Mostramos mensaje.
        Session::flash('mensaje', 'Personaje creado correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('personajes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Personaje $personaje)
    {
        // Conseguimos la informacion del personaje
        return view('personajes.personaje')->with('personaje', $personaje);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Personaje $personaje)
    {
        return view('personajes.edit')->with('personaje', $personaje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePersonajeRequest $request, Personaje $personaje)
    {
        $datosvalidados = $request->validated();

        if ($request->file('imagen')){
            // Borramos la imagen anterior, siempre y cuando esta no sea la imagen que configuramos por defecto
            if($personaje->imagen != 'imagenes/imagen_por_defecto.jpg') { unlink(storage_path('app/public/'.$personaje->imagen)); }

            $imagen = $request->file('imagen');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['imagen'] = $ruta;
        }

        // Actualizamos el personaje.
        $personaje->update($datosvalidados);

        // Mostramos un mensaje.
        Session::flash('mensaje', 'Personaje actualizado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('personajes.show',$personaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personaje $personaje)
    {
        // Borramos la imagen del servidor, siempre y cuando esta no sea la imagen que configuramos por defecto
        if($personaje->imagen != 'imagenes/imagen_por_defecto.jpg') { unlink(storage_path('app/public/'.$personaje->imagen)); }

        // Borramos el registro
        $personaje->delete();

        // Mostramos mensaje
        Session::flash('mensaje', 'Personaje borrado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index
        return redirect()->route('personajes.index');
    }

    //////////////////////////////
    // FUNCIONES PARA LOS MATERIALES
    //////////////////////////////

    public function listadoMateriales()
    {
        $materiales = Material::All();

        return view('personajes.listaMateriales')->with('materiales', $materiales);
    }

    public function createMaterial()
    {
        return view('personajes.createMaterial')->with('material', new Material());
    }

    public function storeMaterial(StoreMaterialRequest $request)
    {
        $datosvalidados = $request->validated();
        $datosvalidados['imagen'] = "imagenes/imagen_por_defecto.jpg";

        if ($request->file('imagen')){
            $imagen = $request->file('imagen');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['imagen'] = $ruta;
        }

        // Creamos el personaje con todos los datos
        Material::create($datosvalidados);

        // Mostramos mensaje.
        Session::flash('mensaje', 'Material creado correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('personajes.listadomateriales');
    }

    public function showMaterial(Material $material)
    {
        // Conseguimos la informacion del material
        return view('personajes.material')->with('material', $material);
    }

    public function editMaterial(Material $material)
    {
        return view('personajes.editMaterial')->with('material', $material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMaterial(StoreMaterialRequest $request, Material $material)
    {
        $datosvalidados = $request->validated();

        if ($request->file('imagen')){
            // Borramos la imagen anterior, siempre y cuando no sea la imagen por defecto
            if($material->imagen != 'imagenes/imagen_por_defecto.jpg') { unlink(storage_path('app/public/'.$material->imagen)); }

            $imagen = $request->file('imagen');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['imagen'] = $ruta;
        }

        // Actualizamos el material.
        $material->update($datosvalidados);

        // Mostramos un mensaje.
        Session::flash('mensaje', 'Material actualizado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('personajes.listadomateriales');
    }

    public function destroyMaterial(Material $material)
    {
        // Borramos la imagen siempre y cuando no sea la imagen por defecto
        if($material->imagen != 'imagenes/imagen_por_defecto.jpg') { unlink(storage_path('app/public/'.$material->imagen)); }

        // Borramos el registro
        $material->delete();

        // Mostramos mensaje
        Session::flash('mensaje', 'Material borrado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index
        return redirect()->route('personajes.listadomateriales');
    }

     //////////////////////////////
    // FUNCIONES PARA LAS ASCENSIONES
    //////////////////////////////

    public function listadoAscensiones()
    {
        $ascensiones = MaterialPersonaje::All();
        $personajes = Personaje::All();
        $materiales = Material::All();

        return view('personajes.listaAscensiones')->with('materiales',$materiales)->with('ascensiones', $ascensiones)->with('personajes', $personajes);
    }

    public function createAscension()
    {
        $materiales = Material::all();
        $personajes = Personaje::all();
        return view('personajes.createAscension')->with('materiales', $materiales)->with('personajes', $personajes)->with('ascension', new MaterialPersonaje());
    }

    public function storeAscension(StoreAscensionRequest $request)
    {

        // Creamos el personaje con todos los datos
        MaterialPersonaje::create( $request->validated());

        // Mostramos mensaje.
        Session::flash('mensaje', 'Ascensión creada correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('personajes.listadoascensiones');
    }

    public function editAscension(MaterialPersonaje $ascension)
    {
        $materiales = Material::all();
        $personajes = Personaje::all();
        return view('personajes.editAscension')->with('materiales', $materiales)->with('personajes', $personajes)->with('ascension', $ascension);
    }

    public function updateAscension(StoreAscensionRequest $request, MaterialPersonaje $ascension)
    {
        // Actualizamos la ascensión.
        $ascension->update($request->validated());

        // Guardamos un mensaje.
        Session::flash('mensaje', 'Ascensión actualizada correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al listado de ascensiones.
        return redirect()->route('personajes.listadoascensiones');
    }

    public function destroyAscension(MaterialPersonaje $ascension)
    {
       // Borramos el registro
        $ascension->delete();

        // guardamos mensaje
        Session::flash('mensaje', 'Ascensión borrada correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index
        return redirect()->route('personajes.listadoascensiones');
    }

}
