<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Post;

class CommentController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth')->except('create');
        $this->middleware('auth.admin')->except('store', 'replyStore');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request)
    {
        $datosvalidados = $request->validated();
        
        $comentario = new Comment();
        $comentario->texto = $datosvalidados['texto'];
        $comentario->user()->associate($request->user());
        $noticia = Post::find($request->noticia_id);
        $noticia->comments()->save($comentario);

        // Recargamos la página
        return back();
    }

    // Respuestas a comentarios
    public function replyStore(StoreCommentRequest $request)
    {
        $datosvalidados = $request->validated();

        //dd($request->comentario_id);
        $respuesta = new Comment();
        $respuesta->texto = $datosvalidados['texto'];
        $respuesta->user()->associate($request->user());
        $respuesta->parent_id = $request->comentario_id;
        $noticia = Post::find($request->noticia_id);
        $noticia->comments()->save($respuesta);

        
        // $datosvalidados['user_id']=auth()->user()->id;
        // Comment::create($datosvalidados);

        // Recargamos la página
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCommentRequest  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommentRequest $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comentario)
    {
        $comentario->delete();

        // Recargamos la página
        return back();
    }
}
