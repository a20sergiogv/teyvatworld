<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;

class UserController extends Controller
{
    public function show(User $usuario)
    {
        // Conseguimos la informacion de la noticia
        return view('auth.profile')->with('usuario', $usuario);
    }

    public function edit(User $usuario)
    {
        return view('auth.edit-user')->with('usuario', $usuario);
    }

    public function update(StoreUserRequest $request, User $usuario)
    {


        $datosvalidados = $request->validated();

        if ($request->file('profile_photo_path')){
            // Borramos la imagen anterior, siempre y cuando esta no sea la imagen que configuramos por defecto
           if($usuario->profile_photo_path != 'imagenes/imagen_por_defecto_usuario.jpg') { unlink(storage_path('app/public/'.$usuario->profile_photo_path)); }

            $imagen = $request->file('profile_photo_path');
            $ruta = $imagen->store('imagenes', 'public');
     
            $datosvalidados['profile_photo_path'] = $ruta;
        } else {
            $datosvalidados['profile_photo_path'] = $usuario->profile_photo_path;
        }

        // Actualizamos el usuario
        $usuario->real_name = $datosvalidados['real_name'];
        $usuario->profile_photo_path = $datosvalidados['profile_photo_path'];
        $usuario->location = $datosvalidados['location'];
        $usuario->hoyo_id = $datosvalidados['hoyo_id'];
        $usuario->birthday = $datosvalidados['birthday'];
        $usuario->save();

        // Redireccionamos al index.
        return redirect()->route('user.profile',$usuario);
    }
}
