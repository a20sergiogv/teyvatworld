<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePersonajeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:30',
            'imagen' => 'nullable|mimes:png,jpg,jpeg|max:2048',
            'titulo' => 'required|max:50',
            'alianza' => 'required|max:50',
            'rareza' => 'required|max:30',
            'elemento' => 'required|max:10',
            'arma' => 'required|max:10',
            'cumple' => 'required|max:30',
            'constelacion' => 'required|max:30',
            'S_J' => 'required|max:30',
            'S_C' => 'required|max:30',
            'S_I' => 'required|max:30',
            'descripcion' => 'required|max:200',
        ];
    }
}
