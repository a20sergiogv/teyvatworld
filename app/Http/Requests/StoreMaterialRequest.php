<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:30',
            'imagen' => 'nullable|mimes:png,jpg,jpeg|max:2048',
            'tipo' => 'required|max:40',
            'rareza' => 'required|max:15',
            'ubicacion' => 'required|max:50',
            'descripcion' => 'required|max:500',
        ];
    }
}
