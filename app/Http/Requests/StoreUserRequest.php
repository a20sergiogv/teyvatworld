<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'real_name' => 'nullable|max:100',
            'profile_photo_path' => 'nullable|mimes:png,jpg,jpeg',
            'location' => 'nullable',
            'hoyo_id' => 'nullable',
            'birthday' => 'nullable',
        ];
    }
}
