<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personajes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 30);
            $table->string('imagen', 150);
            $table->string('titulo', 50);
            $table->string('alianza', 50);
            $table->string('rareza', 30);
            $table->string('arma', 10);
            $table->string('elemento', 10);
            $table->string('cumple', 30);
            $table->string('constelacion', 30);
            $table->string('S_J', 30);
            $table->string('S_C', 30);
            $table->string('S_I', 30);
            $table->string('descripcion', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personajes');
    }
};
