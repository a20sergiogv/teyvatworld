<?php

namespace Database\Factories;

use App\Models\Personaje;
use App\Models\Material;
use App\Models\MaterialPersonaje;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MaterialPersonaje>
 */
class MaterialPersonajeFactory extends Factory
{
    protected $model = MaterialPersonaje::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $totalPersonajes = Personaje::all()->count();
        $totalMateriales = Material::all()->count();
        return [
            'personaje_id' => $this->faker->numberBetween(1, $totalPersonajes),
            'material_id' => $this->faker->numberBetween(1, $totalMateriales),
        ];
    }
}
