<?php

namespace Database\Factories;

use App\Models\Material;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Material>
 */
class MaterialFactory extends Factory
{
    protected $model = Material::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => "nombre aqui",
            'imagen' => "imagenes/imagen_por_defecto.jpg",
            'tipo' => "tipo aqui",
            'rareza' => "rareza aqui",
            'ubicacion' => "ubicación aquí",
            'descripcion' => "descripcion aqui",
        ];
    }
}
