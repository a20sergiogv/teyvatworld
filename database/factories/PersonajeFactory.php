<?php

namespace Database\Factories;

use App\Models\Personaje;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Personaje>
 */
class PersonajeFactory extends Factory
{
    protected $model = Personaje::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'imagen' => "imagenes/imagen_por_defecto.jpg",
            'titulo' =>  $this->faker->text(20),
            'alianza' =>  $this->faker->text(20),
            'rareza' =>  $this->faker->text(20),
            'arma' => $this->faker->text(10),
            'elemento' => "cryo",
            'cumple' => Str::random(10),
            'constelacion' => Str::random(10),
            'S_J' => $this->faker->name(),
            'S_C' => $this->faker->name(),
            'S_I' => $this->faker->name(),
            'descripcion' => $this->faker->text(50),
        ];
    }
}
